# Язык программриования Rust

Есть две редакции книги "The Rust Programming Language":

* [Первая редакция](first-edition/index.html)
* [Вторая редакция](second-edition/index.html)

The second edition is a complete re-write. It is still under construction,
though it is far enough along to learn most of Rust. We suggest reading the
second edition and then checking out the first edition later to pick up some of
the more esoteric parts of the language.
